# The Arabidopsis Information Portal: an Application Platform for Data Discovery

The Arabidopsis Information Portal (AIP) is an open-access online community resource for research on the _Arabidopsis thaliana_ genome and related data. AIP is developed through a partnership between J. Craig Venter Institute, the Texas Advanced Computing Center at The University of Texas at Austin, and The University of Cambridge. Part of the open architecture of AIP is a science applications workspace. Researchers can select applications developed both by the AIP team and the community from an "app store" to construct a customized environment for their work. AIP provides tooling and support for developing applications for AIP including an application generator, interactive development and testing environment, and a straightforward deployment path for deploying an application into the AIP workspace.

Submitted to [GCE '14](http://sciencegateways.org/upcoming-events/gce14/).

Expanded and submitted to [CCPE Special Issue](http://www.cc-pe.net/journalinfo/issues/2014.html#SGW-2014).
